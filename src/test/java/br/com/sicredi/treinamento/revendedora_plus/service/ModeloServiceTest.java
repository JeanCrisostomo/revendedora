package br.com.sicredi.treinamento.revendedora_plus.service;

import br.com.sicredi.treinamento.revendedora_plus.exceptions.ModeloNaoExisteException;
import br.com.sicredi.treinamento.revendedora_plus.model.Modelo;
import br.com.sicredi.treinamento.revendedora_plus.repository.ModeloRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ModeloServiceTest {

    @Mock
    private ModeloRepository modeloRepository;

    @InjectMocks
    private ModeloServiceImpl modeloService;

    @Test
    public void deveriaRetornarModeloTest() throws ModeloNaoExisteException {
        Modelo modelo = new Modelo();
        modelo.setId(1L);
        modelo.setNome("Test");
        when(modeloRepository.findById(1L)).thenReturn(Optional.of(modelo));

        Modelo modeloReturned = modeloService.findById(1L);

        assertEquals(modelo, modeloReturned);
        verify(modeloRepository).findById(1L);
    }


    @Test(expected = ModeloNaoExisteException.class)
    public void deveriaLancarExceptionQuandoModeloNaoExiste() throws ModeloNaoExisteException {
        when(modeloRepository.findById(1L)).thenReturn(Optional.empty());

        modeloService.findById(1L);
    }
}
