package br.com.sicredi.treinamento.revendedora_plus.exceptions;

public class FipeIndisponivelException extends Exception {

    public FipeIndisponivelException(String message) {
        super(message);
    }

}
