package br.com.sicredi.treinamento.revendedora_plus.service;

import br.com.sicredi.treinamento.revendedora_plus.exceptions.ModeloNaoExisteException;
import br.com.sicredi.treinamento.revendedora_plus.model.Modelo;

public interface ModeloService {

  Modelo findById(Long id) throws ModeloNaoExisteException;
}
