package br.com.sicredi.treinamento.revendedora_plus.exceptions.handlers;

import br.com.sicredi.treinamento.revendedora_plus.exceptions.FipeIndisponivelException;
import br.com.sicredi.treinamento.revendedora_plus.exceptions.dto.ApiErrorDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class FipeIndisponivelExceptionHandler {

    @ExceptionHandler(FipeIndisponivelException.class)
    public ResponseEntity<ApiErrorDTO> handle(FipeIndisponivelException ex) {
        return ResponseEntity.status(503).body(process(ex));
    }

    private ApiErrorDTO process(FipeIndisponivelException ex) {
        return ApiErrorDTO.builder()
                .code("fipe_indisponivel")
                .detail(ex.getMessage())
                .build();
    }
}
